using NUnit.Framework;
using NLog;
using System;
using System.IO;
using System.Diagnostics;
using DFUTester;

namespace Tests
{
    public class Tests
    {
        Logger log = LogManager.GetCurrentClassLogger();
        private string DFUExePath = @"C:\docflow\dfu\dfu.exe";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ElabFile_Ok()
        {
            log.Trace("Deposito file in input");
            Guid guid = Guid.NewGuid();
            string inputFileName = string.Format($"D:\\tmp\\file_{guid}.txt");
            File.WriteAllText(inputFileName, guid.ToString());
            Assert.That(File.Exists(inputFileName));

            //Esecuzione DFU
            //Assert.That(File.Exists(DFUExePath));
            log.Trace("Avvio DFU");
            RunDFU(DFUExePath);

            //Verifiche
            Assert.That(!File.Exists(DFUExePath));

        }

        [Test]
        public void ElabRow_Ok()
        {
            Table1 t = new Table1() { Id = 6, Name = "topolino" };

            log.Trace("Caricamento riga da elaborare");
            using (postgresContext db = new postgresContext())
            {
                Table1 tableCurr = db.Table1.Find(t.Id);
                if(tableCurr == null)
                    db.Table1.Add(t);
                else if (!tableCurr.Equals(t))
                {
                    tableCurr.Name = t.Name;
                    db.Table1.Update(tableCurr);
                }
                db.SaveChanges();
            }

            log.Trace("Verifica riga presente");
            using (postgresContext db = new postgresContext())
            {
                Assert.AreEqual(db.Table1.Find(t.Id).Name,t.Name);
            }
        }

        private void RunDFU(string DFUExePath)
        {
            using (var process = new Process())
            {
                //process.StartInfo.FileName = path; //es @"..\HelloWorld\bin\Debug\helloworld.exe"; // relative path. absolute path works too.
                //process.StartInfo.Arguments = ""; //$"{id}";
                process.StartInfo.FileName = @"cmd.exe";
                process.StartInfo.Arguments = @"/c del D:\tmp\file_*.txt";      // print the current working directory information
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                process.OutputDataReceived += (sender, data) => Console.WriteLine(data.Data);
                process.ErrorDataReceived += (sender, data) => Console.WriteLine(data.Data);
                Console.WriteLine("starting");
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                var exited = process.WaitForExit(1000 * 10);     // (optional) wait up to 10 seconds
                Console.WriteLine($"exit {exited}");
            }
        }
    }
}